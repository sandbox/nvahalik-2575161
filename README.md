Commerce Bundle Helper
======================

This module provides helpful tools for easing the process of creating bundles.
For more information, check out the [Commerce Bundle Docs][docs].

It does this by giving you a few tools:

1. A view which has VBO and allows you to automate the creation of bundle items.
   This is important since Commerce Bundle requires that each product bundle 
   group contain product bundle items. So you basically will have to do this.
2. If you're using Commerce Backoffice, this module will automatically add the
   relevant VBO actions to your products and product variations views.
3. A VBO action for creating a bundle group from a product display. This auto-
   mates the creation of the bundle items and setting the references. The 
   bundle SKU defaults to BUNDLE-[product_id], the quantity defaults to "1", 
   and the default title of the bundle is "[product-title] Bundle".
4. Another VBO action for creating a bundle item (used in bundle groups) for an
   individual product. It's used in the product bundler.

The action described in #3 action which can be used to allow 
VBO access to the rule, allowing you to select one or more items and have them 
automatically be duplicated as a bundle. The bundle SKU defaults to 
BUNDLE-[product_id], the quantity defaults to "1" and the default title of the 
bundle is "[product-title] Bundle".

[docs]: https://www.drupal.org/node/2182973