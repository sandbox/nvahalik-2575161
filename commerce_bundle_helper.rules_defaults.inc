<?php

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_bundle_helper_default_rules_configuration() {
  $configs = array();

  $configs['rules_copy_product_to_a_bundle'] = rules_import('{ "rules_copy_product_to_a_bundle" : {
      "LABEL" : "Create Bundle from a Product Display",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "orig_pbd" : { "label" : "Node as passed", "type" : "node" } },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "orig-pbd" ], "field" : "field_product" } }
      ],
      "DO" : [
        { "entity_create" : {
            "USING" : {
              "type" : "commerce_product",
              "param_sku" : "BUNDLE-[orig-pbd:nid]",
              "param_type" : "commerce_bundle_group",
              "param_title" : "[orig-pdb:title] Bundle",
              "param_creator" : [ "site:current-user" ],
              "param_commerce_price" : { "value" : { "amount" : 0, "currency_code" : "USD" } }
            },
            "PROVIDE" : { "entity_created" : { "created_pbg" : "Created Product Bundle Group" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "orig-pbd:field-product" ] },
            "ITEM" : { "orig_pdb_item" : "Current Product Display reference item" },
            "DO" : [
              { "component_rules_create_bundle_item_from_product" : {
                  "USING" : { "orig_product" : [ "orig-pdb-item" ] },
                  "PROVIDE" : { "pbi_int" : { "pbi_int" : "Bundle Item ID" } }
                }
              },
              { "entity_fetch" : {
                  "USING" : { "type" : "commerce_bundle_item", "id" : [ "pbi-int" ] },
                  "PROVIDE" : { "entity_fetched" : { "entity_fetched" : "Fetched entity" } }
                }
              },
              { "list_add" : {
                  "list" : [ "created-pbg:commerce-bundle-items" ],
                  "item" : [ "entity-fetched" ]
                }
              }
            ]
          }
        },
        { "data_set" : {
            "data" : [ "created-pbg:commerce-bundle-unit-quantity" ],
            "value" : "1"
          }
        },
        { "entity_save" : { "data" : [ "created-pbg" ], "immediate" : 1 } }
      ]
    }
  }');

  $configs['rules_create_bundle_item_from_product'] = rules_import('{ "rules_create_bundle_item_from_product" : {
      "LABEL" : "Create Bundle Item from Product",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "orig_product" : { "label" : "Product", "type" : "commerce_product" },
        "pbi_int" : { "label" : "Bundle Item ID", "type" : "integer", "parameter" : false }
      },
      "DO" : [
        { "entity_create" : {
            "USING" : { "type" : "commerce_bundle_item", "param_type" : "bundle_item" },
            "PROVIDE" : { "entity_created" : { "created_pbi" : "Created PBI" } }
          }
        },
        { "data_set" : { "data" : [ "created-pbi:type" ], "value" : "bundle_item" } },
        { "data_set" : {
            "data" : [ "created-pbi:commerce-bundle-product" ],
            "value" : [ "orig-product" ]
          }
        },
        { "data_set" : {
            "data" : [ "created-pbi:commerce-bundle-price" ],
            "value" : [ "orig-product:commerce-price" ]
          }
        },
        { "data_set" : { "data" : [ "created-pbi:status" ], "value" : 1 } },
        { "entity_save" : { "data" : [ "created-pbi" ], "immediate" : 1 } },
        { "data_set" : { "data" : [ "pbi-int" ], "value" : [ "created-pbi:item-id" ] } }
      ],
      "PROVIDES VARIABLES" : [ "pbi_int" ]
    }
  }');

  $configs['rules_create_a_bundle_from_a_node'] = rules_import('{ "rules_create_a_bundle_from_a_node" : {
      "LABEL" : "Copy into Bundle",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "ACTION SET" : [
        { "component_rules_copy_product_to_a_bundle" : { "orig_pbd" : [ "node" ] } }
      ]
    }
  }');

  return $configs;
}
